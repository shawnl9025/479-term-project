// TODO remove comments and escape \ characters when copying

function getIndexInParent(node) {
    if (!node.parentElement) return -1;
    for (let i = 0; i < node.parentElement.childNodes.length; ++i) {
        if (node.parentElement.childNodes[i] === node) {
            return i;
        }
    }
    return -1;
}

function compareNode(node, rep) {
    if (node.nodeType !== Node.ELEMENT_NODE) { return 0; }

    let totalAttrs = 0;
    let matchedAttrs = 0;

    totalAttrs += 4;
    if (node.tagName.toLowerCase() === rep.tagName)++matchedAttrs;
    if (node.childNodes.length === rep.numChildren)++matchedAttrs;
    if (node.parentElement) {
        if (rep.indexInParent === getIndexInParent(node))++matchedAttrs;
        if (node.parentElement.tagName.toLowerCase() === rep.parentTagName)++matchedAttrs;
    } else {
        if (rep.indexInParent == undefined)++matchedAttrs;
        if (rep.parentTagName == undefined)++matchedAttrs;
    }

    totalAttrs += node.attributes.length;
    for (let i = 0; i < node.attributes.length; ++i) {
        let attribute = node.attributes[i];
        if (rep.attributes[attribute.name] === attribute.value)++matchedAttrs;
    }

    for (let key in rep.attributes) {
        if (!(key in node.attributes)) {
            ++matchedAttrs;
        }
    }

    return matchedAttrs / totalAttrs;
}

// TODO add some more match modes
function findNode(node, minNodeSimilarity) {
    let reps = node.path;

    let curNode = document.documentElement;

    let directRep = true;

    for (let i = 1; i < reps.length; ++i) {
        let nextRep = reps[i];

        let maxSimilarity = 0;
        let maxSimilarityNode

        for (let j = 0; j < curNode.childNodes.length; ++j) {
            let nextNode = curNode.childNodes[j];

            let similarity = compareNode(nextNode, nextRep);
            if (similarity >= maxSimilarity) {
                maxSimilarity = similarity;
                maxSimilarityNode = nextNode;
            }
        }

        if (maxSimilarity >= minNodeSimilarity) {
            curNode = maxSimilarityNode;
        } else {
            continue;
        }

        if (i === reps.length - 1) {
            return curNode;
        }
    }

    return null;
}

function scrapeText(node) {
    return node.textContent;
}

let canvas = document.createElement('canvas');

function scrapeImage(node) {
    if (node.tagName.toLowerCase() !== 'img') return '';

    canvas.width = node.width;
    canvas.height = node.height;

    let ctx = canvas.getContext("2d");
    ctx.drawImage(node, 0, 0);

    let dataURL = canvas.toDataURL('image/png');
    return dataURL.replace(/^data:image\/(png|jpg);base64,/, '');
}

function generalizeScrape(node, scrape) {
    let results = [];

    if ((node.tagName.toLowerCase() === 'th' || node.tagName.toLowerCase() === 'td') && node.parentElement) {
        let parent = node.parentElement;
        let index = getIndexInParent(node);

        if (index !== -1) {
            let curNode = parent.previousSibling;
            while (curNode) {
                if (curNode.childNodes.length > index) results.push(scrape(curNode.childNodes[index]));
                curNode = curNode.previousSibling;
            }

            curNode = parent.nextSibling;
            while (curNode) {
                if (curNode.childNodes.length > index) results.push(scrape(curNode.childNodes[index]));
                curNode = curNode.nextSibling;
            }
        }
    }

    results.push(scrape(node));

    return results;
}

function wait(node) {
    return new Promise((resolve, reject) => {
        const config = { attributes: true, childList: true, subtree: true };

        let observer;

        const callback = (mutationsList, observer) => {
            resolve();
            observer.disconnect();
        };

        observer = new MutationObserver(callback);
        observer.observe(node, config);
    });
}