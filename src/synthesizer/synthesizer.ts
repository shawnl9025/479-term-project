import { Action } from '../ir/action'
import { Settings } from '../ir/settings'
import { default as jsp } from './jsp-synthesizer'

/**
 * does NOT work for string enums
 */
function enumToArray(e): any {
    let keys = [];
    for (let key in e) {
        if (!isNaN(Number(key))) {
            keys.push(key)
        }
    }
    return keys
}

export enum SupportedLanguage {
    Javascript
}

export const AllSupportedLanguages: Array<SupportedLanguage> = enumToArray(SupportedLanguage)

/**
 * user expects this enum to start with 0
 */
export enum SupportedLibrary {
    Puppeteer
}

export function getSupportedLibrary(language: SupportedLanguage): Array<SupportedLibrary> {
    switch (language) {
        case SupportedLanguage.Javascript: {
            return [
                SupportedLibrary.Puppeteer,
            ]
        }
    }

    throw new Error(`language ${language} is not supported`)
}

function throwNotSupported(language: SupportedLanguage, library: SupportedLibrary) {
}

export function synthesize(actions: Action[], settings: Settings, language: SupportedLanguage, library: SupportedLibrary): string {
    if (!getSupportedLibrary(language).includes(library)) throw new Error(`library ${library} is not supported for language ${language}`)

    let supported = true
    let result = ''

    switch (language) {
        case SupportedLanguage.Javascript: {
            switch (library) {
                case SupportedLibrary.Puppeteer: {
                    return jsp(actions, settings)
                }

                default: {
                    supported = false
                    break
                }
            }
            break
        }

        default: {
            supported = false
            break
        }
    }

    if (!supported) throw new Error(`language ${SupportedLanguage[language]}, library ${SupportedLibrary[library]} is not supported`)

    return result
}