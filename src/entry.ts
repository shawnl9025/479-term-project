import { default as settings } from './ir/settings'
import { BackgroundFacet } from './background-facet'
import { MessageType, SelectNodeResponse } from './message-type'
import { NodeIdentifier } from './ir/node'
import { ActionType, Click, ActionListMap, ScrapeType, ScrapeText, ScrapeAttribute, ScrapeImage, WaitType, WaitNode, WaitAttribute } from './ir/action'

// expose functionality to popup
let actionListMap: ActionListMap = new Map()

null;
(function () {
    let backgroundFacet = new BackgroundFacet()
    backgroundFacet.settings = settings
    backgroundFacet.actionListMap = actionListMap
    backgroundFacet.sanityCheck = () => console.log("sanity check")
    backgroundFacet.log = console.log

    null;
    (window as any).backgroundFacet = backgroundFacet
}())

/////////////////////
// handlers on content-script response
/////////////////////
function getFirstAttributeName(node: NodeIdentifier): string | undefined {
    const elementAttributes = node.path[node.path.length - 1].attributes
    const attributeNames = Object.keys(elementAttributes)
    if (attributeNames.length === 0) return undefined
    else return attributeNames[0]
}

chrome.runtime.onMessage.addListener((message: SelectNodeResponse, sender, callback) => {
    switch (message.id) {
        case MessageType.SelectNodeResponse: {
            const node: NodeIdentifier = message.node
            if (!node) return

            let actionList = actionListMap.get(sender.tab.id)
            if (actionList == undefined) {
                actionList = []
                actionListMap.set(sender.tab.id, actionList)
            }

            switch (message.primaryActionType) {
                case ActionType.Click: {
                    actionList.push(new Click(node))
                    break
                }

                case ActionType.Scrape: {
                    switch (message.secondaryActionType) {
                        case ScrapeType.Text: {
                            actionList.push(new ScrapeText(node))
                            break
                        }
                        case ScrapeType.Attribute: {
                            let firstAttributeName = getFirstAttributeName(node)
                            if(!firstAttributeName) return
                            // TODO alert user that the action is not added because element does not have an attribute
                            actionList.push(new ScrapeAttribute(node, firstAttributeName))
                            break
                        }
                        case ScrapeType.Image: {
                            actionList.push(new ScrapeImage(node))
                            break
                        }
                    }
                    break
                }

                case ActionType.Wait: {
                    switch (message.secondaryActionType) {
                        case WaitType.Node: {
                            actionList.push(new WaitNode(node))
                            break
                        }
                        
                        case WaitType.Attribute: {
                            let firstAttributeName = getFirstAttributeName(node)
                            if(!firstAttributeName) return
                            // TODO alert user that the action is not added because element does not have an attribute
                            actionList.push(new WaitAttribute(node, firstAttributeName))
                            break
                        }
                    }
                    break
                }
            }

            break
        }
    }
})

/////////////////////
// main entry to extension
/////////////////////
chrome.runtime.onStartup.addListener(() => {
    settings.loadSettings()
})