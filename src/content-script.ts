import { MessageType, SelectNodeRequest, SelectNodeResponse } from './message-type'
import { NodeIdentifier, NodeRepresentation } from './ir/node'
import { Settings } from './ir/settings'

let overlay: HTMLElement
let currentOverlaidElement: HTMLElement
let currentRequest: SelectNodeRequest
let overlaying: boolean = false
let cursorClientX, cursorClientY

/////////////////////
// overlay
/////////////////////
function resetOverlayStyles() {
    overlay.style.width = '0px'
    overlay.style.height = '0px'
    overlay.style.position = 'fixed'
    overlay.style.zIndex = '2147483647'
    overlay.style.backgroundColor = '#0099CC'
    overlay.style.opacity = '0.5'
}

function overlayNode(e: HTMLElement) {
    if (e === overlay || !e.getBoundingClientRect) return
    let targetPosition = e.getBoundingClientRect()

    overlay.style.width = `${targetPosition.width}px`
    overlay.style.height = `${targetPosition.height}px`
    overlay.style.left = `${targetPosition.left}px`
    overlay.style.top = `${targetPosition.top}px`

    currentOverlaidElement = e
}

function overlayCursorNode() {
    if (cursorClientX == undefined || cursorClientY == undefined) return

    resetOverlayStyles()
    let node = document.elementFromPoint(cursorClientX, cursorClientY)
    overlayNode(node as HTMLElement)
}

function initializeOverlay() {
    overlay = document.createElement('div')

    document.body.appendChild(overlay)
    resetOverlayStyles()
}

/////////////////////
// event handlers
/////////////////////
function onMouseMove(e: MouseEvent) {
    cursorClientX = e.clientX
    cursorClientY = e.clientY

    overlayCursorNode()
}

function onScroll(e: Event) {
    overlayCursorNode()
}

function onClick(e: Event) {
    if (e.target !== overlay) return

    respond(NodeIdentifier.makeNodeIdentifier(currentOverlaidElement, currentRequest.settings))

    exitSelectNode()
}

function onKeydown(e: KeyboardEvent) {
    if (overlaying && e.key === 'Escape') {
        exitSelectNode()
    }
}

function registerListeners() {
    document.addEventListener('keydown', onKeydown)
    document.addEventListener('mousemove', onMouseMove)
    document.addEventListener('scroll', onScroll)
    document.addEventListener('click', onClick)
}

function unregisterListeners() {
    document.removeEventListener('keydown', onKeydown)
    document.removeEventListener('mousemove', onMouseMove)
    document.removeEventListener('scroll', onScroll)
    document.removeEventListener('click', onClick)
}

/////////////////////
// message handler
/////////////////////
function respond(value?: NodeIdentifier) {
    let response: SelectNodeResponse = {
        id: MessageType.SelectNodeResponse,
        node: value,
        primaryActionType: currentRequest.primaryActionType,
        secondaryActionType: currentRequest.secondaryActionType,
    }

    chrome.runtime.sendMessage(response)
}

function selectNode() {
    overlaying = true
    registerListeners()
}

function exitSelectNode() {
    unregisterListeners()
    resetOverlayStyles()
    overlaying = false
}

/////////////////////
// content-script entry
/////////////////////
chrome.runtime.onMessage.addListener((message, sender, _callback) => {
    if (message.id !== MessageType.SelectNodeRequest) return
    currentRequest = message
    selectNode()
})

initializeOverlay()