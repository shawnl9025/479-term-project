import { ActionListItem } from "./action";
import { Settings } from "./settings";

export class IR {
    actionList: Array<ActionListItem>
    settings: Settings

    constructor(actionList: Array<ActionListItem>, settings: Settings) {
        this.actionList = actionList
        this.settings = settings
    }
}