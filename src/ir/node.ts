import { Settings } from './settings'

export class NodeRepresentation {
    tagName: string
    attributes: Record<string, string>
    numChildren: number // number of nodes, not elements
    indexInParent?: number // index in terms of nodes
    parentTagName?: string

    constructor(tagName: string, attributes: Record<string, string>, numChildren: number, indexInParent?: number, parentTagName?: string) {
        this.tagName = tagName.toLowerCase()
        this.attributes = attributes
        this.numChildren = numChildren
        this.indexInParent = indexInParent
        this.parentTagName = parentTagName
    }

    static makeNodeRepresentation(e: HTMLElement, settings: Settings): NodeRepresentation {
        let recordedAttributes: Record<string, string> = {}

        let attributesCount = 0
        let attributesSizeInByte = 0

        for (let i = 0; i < e.attributes.length && attributesCount <= settings.maxAttribute && attributesSizeInByte <= settings.maxAttributeSizeInByte; ++i) {
            let attributeName = e.attributes[i].name
            let attributeValue = e.attributes[i].value

            recordedAttributes[attributeName] = attributeValue

            ++attributesCount
            attributesSizeInByte += (new TextEncoder().encode(attributeName)).length
            attributesSizeInByte += (new TextEncoder().encode(attributeValue)).length
        }

        let parent = e.parentElement

        let noderep = new NodeRepresentation(e.tagName.toLowerCase(), recordedAttributes, e.childNodes.length)
        if (!parent) return noderep
        
        noderep.parentTagName = parent.tagName.toLowerCase()
        for(let i = 0; i < parent.childNodes.length; ++i){
            if(parent.childNodes[i] === e){
                noderep.indexInParent = i
                break
            }
        }

        return noderep
    }
}

export class NodeIdentifier {
    path: NodeRepresentation[]

    constructor(path: NodeRepresentation[]) {
        this.path = path
    }

    static makeNodeIdentifier(e: HTMLElement, settings: Settings): NodeIdentifier {
        let path: NodeRepresentation[] = []
        let curNode = e

        while (curNode && curNode instanceof HTMLElement) {
            path.unshift(NodeRepresentation.makeNodeRepresentation(curNode, settings))
            curNode = (curNode.parentNode as HTMLElement)
        }

        return new NodeIdentifier(path)
    }
}