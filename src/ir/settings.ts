export class Settings {
    maxAttribute: number
    maxAttributeSizeInByte: number
    maxWaitTimeSec: number
    minNodeSimilarity: number
}

const DEFAULT_SETTINGS = {
    maxAttribute: 100,
    maxAttributeSizeInByte: 1024,
    maxWaitTimeSec: 3,
    minNodeSimilarity: 0.5, 
}

let settings: Settings = { ...DEFAULT_SETTINGS }

const SETTINGS_KEY = 'settings'

export class SettingsManager {
    getMaxAttribute(): number {
        return settings.maxAttribute
    }
    setMaxAttribute(maxAttribute: number) {
        settings.maxAttribute = maxAttribute
        this.saveSettings()
    }

    getMaxAttributeSize(): number {
        return settings.maxAttributeSizeInByte
    }
    setMaxAttributeSize(maxAttributeSize: number) {
        settings.maxAttributeSizeInByte = maxAttributeSize
        this.saveSettings()
    }

    getMaxWaitTimeSec(): number {
        return settings.maxWaitTimeSec
    }
    setMaxWaitTimeSec(maxWaitTimeSec: number) {
        settings.maxWaitTimeSec = maxWaitTimeSec
        this.saveSettings()
    }

    saveSettings() {
        chrome.storage.sync.set({ [SETTINGS_KEY]: settings })
    }

    loadSettings() {
        chrome.storage.sync.get([SETTINGS_KEY], result => {
            settings = { ...settings, ...result[SETTINGS_KEY] }
        })
    }

    /**
     * should only be used in a readonly fashion (e.g. passing it in a message)
     */
    getSettings(): Settings {
        return settings
    }
}

export default new SettingsManager()