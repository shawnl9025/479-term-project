import { NodeIdentifier } from './node'

export type ActionListItem = Action // TODO implement action group

export type ActionListMap = Map<number, Array<ActionListItem>> // map from tabid to action list

export enum ActionType {
    Click, SendKey, SendString, Wait, Scrape
}

export interface Action {
    type: ActionType
}

export class Click implements Action {
    type = ActionType.Click
    node: NodeIdentifier

    constructor(node: NodeIdentifier) {
        this.node = node
    }
}

export class SendKey implements Action {
    type = ActionType.SendKey
    node: NodeIdentifier
    keycode: number

    constructor(node: NodeIdentifier, keycode: number) {
        this.node = node
        this.keycode = keycode
    }
}

export class SendString implements Action {
    type = ActionType.SendString
    node: NodeIdentifier
    inputString: string

    constructor(node: NodeIdentifier, inputString: string) {
        this.node = node
        this.inputString = inputString
    }
}

export enum WaitType {
    Node, Attribute
}

export class Wait implements Action {
    type = ActionType.Wait
    node: NodeIdentifier
    waitType: WaitType

    protected constructor(node: NodeIdentifier) {
        this.node = node
    }
}

export class WaitNode extends Wait {
    waitType = WaitType.Node
    node: NodeIdentifier

    constructor(node: NodeIdentifier) { super(node) }
}

export class WaitAttribute extends Wait {
    waitType = WaitType.Attribute
    node: NodeIdentifier
    attributeName: string

    constructor(node: NodeIdentifier, attributeName: string) {
        super(node)
        this.attributeName = attributeName
    }
}

export enum ScrapeType {
    Text, Attribute, Image
}

export class Scrape implements Action {
    type = ActionType.Scrape
    scrapeType: ScrapeType
    node: NodeIdentifier

    protected constructor(node: NodeIdentifier) {
        this.node = node
    }
}

export class ScrapeText extends Scrape {
    scrapeType = ScrapeType.Text

    constructor(node: NodeIdentifier) { super(node) }
}

export class ScrapeAttribute extends Scrape {
    scrapeType = ScrapeType.Attribute
    attributeName: string

    constructor(node: NodeIdentifier, attributeName: string) {
        super(node)
        this.attributeName = attributeName
    }
}

export class ScrapeImage extends Scrape {
    scrapeType = ScrapeType.Image

    constructor(node: NodeIdentifier) { super(node) }
}