import { SettingsManager } from './ir/settings'
import { ActionListMap } from './ir/action'

export class BackgroundFacet {
    settings: SettingsManager
    actionListMap: ActionListMap
    sanityCheck: () => void
    log: (msg: string) => void
}