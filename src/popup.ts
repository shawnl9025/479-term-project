import { AllSupportedLanguages, SupportedLanguage, SupportedLibrary, getSupportedLibrary, synthesize } from './synthesizer/synthesizer'
import { MessageType, SelectNodeRequest } from './message-type'
import { BackgroundFacet } from './background-facet'
import { NodeIdentifier } from './ir/node'
import { Action, ActionType, ScrapeType, ActionListItem, Scrape, WaitType, Wait } from './ir/action'
import { getCurrentActiveTab } from './utility/tab'
import { IR } from './ir/ir'

const backgroundFacet: BackgroundFacet = (chrome.extension.getBackgroundPage() as any).backgroundFacet

/////////////////////
// action handlers
/////////////////////
function sendRequest(primaryActionType: number, secondaryActionType: number | undefined) {
    let request: SelectNodeRequest = {
        id: MessageType.SelectNodeRequest,
        settings: backgroundFacet.settings.getSettings(),
        primaryActionType: primaryActionType,
        secondaryActionType: secondaryActionType,
    }

    getCurrentActiveTab().then(tab => chrome.tabs.sendMessage(tab.id, request))
}

function registerActionButtonHandler(primaryActionType: number, secondaryActionType: number | undefined, id: string) {
    document.getElementById(id).addEventListener('click', () => {
        sendRequest(primaryActionType, secondaryActionType)
        window.close()
    })
}

function registerActionButtonHandlers() {
    registerActionButtonHandler(ActionType.Click, undefined, 'clickActionButton')
    registerActionButtonHandler(ActionType.Scrape, ScrapeType.Text, 'scrapeTextActionButton')
    registerActionButtonHandler(ActionType.Scrape, ScrapeType.Attribute, 'scrapeAttributeActionButton')
    registerActionButtonHandler(ActionType.Scrape, ScrapeType.Image, 'scrapeImageActionButton')
    registerActionButtonHandler(ActionType.Wait, WaitType.Node, 'waitNodeActionButton')
    registerActionButtonHandler(ActionType.Wait, WaitType.Node, 'waitAttributeActionButton')
}

/////////////////////
// output handlers
/////////////////////
function download(filename, text) {
    var element = document.createElement('a');
    element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
    element.setAttribute('download', filename);

    element.style.display = 'none';
    document.body.appendChild(element);

    element.click();

    document.body.removeChild(element);
}

function registerOutputIRHandler() {
    document.getElementById('outputIRButton').addEventListener('click', e => {
        getActionList().then(actionList => {
            if (!actionList) actionList = []
            download('IR.txt', JSON.stringify(new IR(actionList, backgroundFacet.settings.getSettings())))
        })
    })
}

function getLanguage() {
    return (document.getElementById('codeLanguageSelect') as HTMLSelectElement).value;
}

function getLibrary() {
    return (document.getElementById('codeLibrarySelect') as HTMLSelectElement).value;
}

function registerOutputTargetChangeHandler() {
    let languageSelect: HTMLSelectElement = document.getElementById('codeLanguageSelect') as HTMLSelectElement
    let librarySelect: HTMLSelectElement = document.getElementById('codeLibrarySelect') as HTMLSelectElement

    languageSelect.addEventListener('change', e => {
        let selectedLanguage = Number(languageSelect.value)

        // get and populate select with options
        let supportedLibraries = getSupportedLibrary(selectedLanguage)
        while (librarySelect.lastChild) { librarySelect.removeChild(librarySelect.lastChild) }
        for (let library of supportedLibraries) {
            librarySelect.appendChild(new Option(SupportedLibrary[library], library.toString()))
        }

        // set value to first
        librarySelect.value = '0'
    })
}

function registerOutputCodeHandler() {
    document.getElementById('outputCodeButton').addEventListener('click', e => {
        getActionList().then(actionList => {
            let code = synthesize(actionList, backgroundFacet.settings.getSettings(), Number(getLanguage()), Number(getLibrary()))
            download('code.js', code)
        })
    })
}

function initializeOutputTargets() {
    let languageSelect: HTMLSelectElement = document.getElementById('codeLanguageSelect') as HTMLSelectElement

    // populate supported languages
    for (let language of AllSupportedLanguages) {
        languageSelect.appendChild(new Option(SupportedLanguage[language], language.toString()))
    }

    // default to javascript
    languageSelect.value = SupportedLanguage.Javascript.toString()

    // manually trigger change event
    languageSelect.dispatchEvent(new Event('change', { bubbles: true }))
}

/////////////////////
// action list
/////////////////////
async function getActionList() {
    return getCurrentActiveTab().then(tab => backgroundFacet.actionListMap.get(tab.id))
}

function createIconElement(type: ActionType) {
    let i = document.createElement('i')

    switch (type) {
        case ActionType.Click: {
            i.className = 'fas fa-hand-point-up'
            break
        }
        case ActionType.Scrape: {
            i.className = 'fas fa-file-medical'
            break
        }
        case ActionType.Wait: {
            i.className = 'fas fa-clock'
            break
        }
    }

    return i
}

function emplaceRemoveButton(li: HTMLLIElement, actionList: Array<ActionListItem>, i: number) {
    let buttonText = document.createElement('i')
    buttonText.className = 'fas fa-times'

    let removeButton = document.createElement('button')
    removeButton.appendChild(buttonText)
    removeButton.style.textAlign = 'center'
    removeButton.style.width = '2rem'
    removeButton.style.height = '2rem'
    removeButton.style.borderRadius = '1rem'
    removeButton.style.border = 'none'
    removeButton.style.background = 'none'
    removeButton.style.marginLeft = 'auto'

    removeButton.addEventListener('click', e => {
        // remove from frontend, global store and rerender if necessary
        li.parentElement.removeChild(li)
        actionList.splice(i, 1)
        if (actionList.length === 0) renderActionList()
    })

    li.appendChild(removeButton)

    return removeButton
}

const nbsp = `${String.fromCharCode(160)}`

function emplaceListItemContent(li: HTMLLIElement, action: Action) {
    li.appendChild(createIconElement(action.type))

    let text = nbsp + nbsp + ActionType[action.type]
    switch (action.type) {
        case ActionType.Scrape: {
            let scrapeAction = (action as Scrape)
            text += nbsp + ScrapeType[scrapeAction.scrapeType]

            if (scrapeAction.scrapeType === ScrapeType.Attribute) {
                // TODO show and allow user to edit attribute name
            }

            break
        }

        case ActionType.Wait: {
            let waitAction = (action as Wait)
            text += nbsp

            switch (waitAction.waitType) {
                case WaitType.Node: {
                    text += "For Node To Change"
                    break
                }

                case WaitType.Node: {
                    text += "For Attribute To Change"
                    // TODO show and allow user to edit attribute name
                    break
                }
            }

            break
        }
    }

    li.appendChild(document.createTextNode(text))
}

function addListItemAttributes(li: HTMLLIElement) {
    li.classList.add('list-group-item')

    li.style.display = 'flex'
    li.style.alignItems = 'center'
    li.style.padding = '0.5rem'
    li.style.paddingLeft = '1rem'
    li.style.paddingRight = '1rem'
}

function registerListItemHandler(li: HTMLLIElement) {
    // change background on hover
    li.addEventListener('mouseenter', e => {
        li.classList.add('bg-primary')
    })
    li.addEventListener('mouseleave', e => {
        li.classList.remove('bg-primary')
    })
}

function renderActionList() {
    let ul = document.getElementById('action-list')

    getActionList().then((actionList: Array<ActionListItem>) => {
        if (!actionList || actionList.length === 0) {
            let div = document.createElement('div')
            div.textContent = 'No actions has been recorded'
            div.id = 'emptyListIndicator'
            ul.appendChild(div)
        } else {
            for (let i = 0; i < actionList.length; ++i) {
                const action = actionList[i]

                let li = document.createElement('li')

                emplaceListItemContent(li, action)
                emplaceRemoveButton(li, actionList, i)
                addListItemAttributes(li)
                registerListItemHandler(li)

                ul.appendChild(li)
            }
        }
    })
}

/////////////////////
// popup entry
/////////////////////
document.addEventListener('DOMContentLoaded', event => {
    renderActionList()

    registerActionButtonHandlers()

    // TODO move to extension, dont store selection here
    registerOutputIRHandler()
    registerOutputTargetChangeHandler()
    registerOutputCodeHandler()
    initializeOutputTargets()
});