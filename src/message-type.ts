import { Settings } from './ir/settings'
import { NodeIdentifier } from './ir/node'
import { ActionType } from './ir/action'

export enum MessageType {
    /**
     * protocol:
     *  popup sends this request to content script upon action button click
     *  content script, upon receiving this request, guides the user to select a node by highlighting the node below user's cursor
     *  upon acquired the user selection, respond to background
     */
    SelectNodeRequest,
    SelectNodeResponse,
}

export interface SelectNodeRequest {
    id: MessageType
    settings: Settings
    primaryActionType: number
    secondaryActionType?: number
}

export interface SelectNodeResponse {
    id: MessageType
    node?: NodeIdentifier
    primaryActionType: number
    secondaryActionType?: number
}