////////////
// file structure
////////////

/*
public/
    popup.html
        defines the DOM structure of the popup
        includes popup.js
    
    manifest.json

popup.ts
    see UI

content-script.ts

entry.ts (background script)
    chrome.runtime.onStartup
        load settings from storage

ir/
    node.ts

    action.ts
        export actions
        see Action

    settings.ts
        export settings
        see Settings

synthesizer/
    synthesizer.ts

    library specific implementations ...

*/

/**
 *  supported acions:
 * 
 *  Click <node>
 *      click on node
 *      
 *  SendKey <node> <keycode>
 *      send key to node
 *  
 *  SendString <node> <string>
 * 
 *  Wait <node> | <node.attr>
 *      wait for the node's content to change, or for the node's attribute to change
 * 
 *  Scrape <node.text> | <node.attr> | <node.img> <name>
 *      scrape information (text, or attribute, or image) and save into a variable with name <name>
 */

////////////
// UI
////////////

/**
 * popup.html:
 *  top row
 *      settings wheel
 *          when clicked, display an overlay of settings
 *
 *  list view
 *      actions
 *          each action on a row, with option to be selected and deleted
 *          when selected, move the insert indicator to before the element
 *
 *      insert indicator
 *          indicates where the next action will be inserted
 *
 *  bottom panel of buttons corresponding to actions
 *      each button has a corresponding handler that is called when the button is clicked
 *      when called, the handler should send a message to content script to begin capturing node
 *      upon receiving the node information, create corresponding IR object and add to global actions array
 *      
 *  output button
 */

////////////
// content-script
////////////

/*
listen for messages to capture node

insert an overlay with 0 width and 0 height with position: fixed and z-index \inf
listen for mousemove event on document (possibly using capture)
listen for mouseclick event on document (possibly using capture)
upon mousemove
    use e.target to select the node under the cursor
    adjust overlay's position, width and height to cover the node

upon mouseclick
    if overlay is being clicked, report back the node (should check this is the same node)

upon user click, send message back to handler with information about the node
*/

////////////
// message system
////////////

/*
the message object must contain a property 'id' of type MessageType, which should be defined in the listener's module to differentiate messages
*/

////////////
// output
////////////

/*
entry point to output layer
should be a big switch statement delegating to specific library implementations
*/